const Koa = require('koa')
const app = new Koa()
const koaStatic = require('koa-static')
const { resolveApp } = require('./utils')
// const history = require('koa-connect-history-api-fallback')
const { historyApiFallback } = require('koa2-connect-history-api-fallback')
const distDir = resolveApp('../../dist')

// 支持history 模式
app.use(
  historyApiFallback({
    whiteList: ['/api'],
  }),
)

console.log(distDir)
const port = 1991
app.use(koaStatic(distDir))

app.listen(port, () => {
  console.log(`服务器已启动，http://localhost:${port}`)
})
