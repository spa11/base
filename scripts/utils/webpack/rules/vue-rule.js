const paths = require('../../paths')
const { isDev, isPro } = require('../../env')

// 处理.vue文件
exports.getVueRule = () => {
  return {
    test: /\.vue$/,
    include: [paths.appSrc],
    use: [
      {
        loader: require.resolve('vue-loader'),
        options: {
          compilerOptions: {
            whitespace: 'condense',
          },
        },
      },
    ],
  }
}
