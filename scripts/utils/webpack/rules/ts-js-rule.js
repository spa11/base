const paths = require('../../paths')
const { isDev, isPro } = require('../../env')
const babelConfig = require('../babel').getBebelConfig()
const babelLoaderOptions = {
  ...babelConfig,
  // This is a feature of `babel-loader` for webpack (not Babel itself).
  // It enables caching results in ./node_modules/.cache/babel-loader/
  // directory for faster rebuilds.
  cacheDirectory: true,
  // See #6846 for context on why cacheCompression is disabled
  cacheCompression: false,
  compact: false,
}
// JavaScript配置
exports.getJsRule = () => {
  return {
    test: /\.(js|jsx)$/,
    include: paths.appSrc,
    use: [
      {
        loader: require.resolve('babel-loader'),
        options: babelLoaderOptions,
      },
    ],
  }
}

// typescript配置
exports.getTsRule = () => {
  return {
    test: /\.(ts|tsx)$/,
    include: paths.appSrc,
    use: [
      {
        loader: require.resolve('babel-loader'),
        options: babelLoaderOptions,
      },
      {
        loader: require.resolve('ts-loader'),
        options: {
          transpileOnly: true,
          happyPackMode: isPro, // 开启多线程打包
          appendTsxSuffixTo: ['\\.vue$'],
        },
      },
    ],
  }
}
