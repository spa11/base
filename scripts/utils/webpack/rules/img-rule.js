const paths = require('../../paths')
const { isDev, isPro } = require('../../env')
// 处理图片模块
exports.getImgRule = () => {
  return {
    test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
    loader: require.resolve('url-loader'),
    options: {
      limit: '10000',
      name: 'static/media/[name].[hash:8].[ext]',
    },
  }
}
