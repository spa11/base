const paths = require('../../paths')
const { isDev, isPro } = require('../../env')

// 处理less模块
exports.getLessRule = () => {
  const miniCssLoader = {
    loader: require('mini-css-extract-plugin').loader,
    // options: {
    //   hmr: false,
    //   publicPath: "../",
    // },
  }
  const styleLoader = { loader: require.resolve('style-loader') }
  const cssLoader = {
    loader: require.resolve('css-loader'),
    options: {
      sourceMap: false,
      esModule: false,
      modules: {
        localIdentName: '[name]_[local]_[hash:base64:5]',
      },
      importLoaders: 2,
    },
  }
  const postcssLoader = {
    loader: require.resolve('postcss-loader'),
    options: {
      sourceMap: false,
      postcssOptions: {
        plugins: [require('autoprefixer')()],
      },
    },
  }
  const lessLoader = {
    loader: require.resolve('less-loader'),
    options: {
      sourceMap: false,
    },
  }

  return {
    test: /\.less$/,
    include: [paths.appSrc],
    oneOf: [
      {
        // 模块化less配置  index.module.less
        test: /\.module\.\w+$/,
        use: [
          isDev ? styleLoader : miniCssLoader,
          cssLoader,
          postcssLoader,
          lessLoader,
        ],
      },
      {
        // vue.less?module
        // .vue 文件里面的 模块化 less
        resourceQuery: /module/,
        use: [
          isDev ? styleLoader : miniCssLoader,
          cssLoader,
          postcssLoader,
          lessLoader,
        ],
      },
      {
        // 普通的less 文件配置
        use: [
          isDev ? styleLoader : miniCssLoader,
          {
            loader: require.resolve('css-loader'),
            options: {
              sourceMap: false,
              esModule: false,
              modules: false,
              importLoaders: 2,
            },
          },
          postcssLoader,
          lessLoader,
        ],
      },
    ],
  }
}
