const paths = require('../../paths')
const { isDev, isPro } = require('../../env')
// 处理svg模块
exports.getSvgRule = () => {
  return {
    test: /\.svg$/,

    oneOf: [
      {
        exclude: [paths.appSvg],
        use: [
          {
            loader: require.resolve('babel-loader'),
          },
          {
            loader: require.resolve('vue-svg-loader'),
            options: {
              svgo: {
                plugins: [
                  { removeDoctype: true },
                  { removeComments: true },
                  { removeViewBox: false },
                ],
                removeViewBox: false,
              },
            },
          },
        ],
      },
      {
        include: [paths.appSvg],
        use: [
          {
            loader: require.resolve('svg-sprite-loader'),
            options: {
              symbolId: 'icon-[name]',
              // extract: true, // 单独文件导出
              // spriteFilename: (svgPath) => `sprite${svgPath.substr(-4)}`,
            },
          },
        ],
      },
    ],
  }
}
