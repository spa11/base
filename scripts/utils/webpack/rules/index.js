// const paths = require('../../paths')
// const { isDev, isPro } = require('../../env')

module.exports = {
  ...require('./ts-js-rule'),
  ...require('./less-rule'),
  ...require('./vue-rule'),
  ...require('./svg-rule'),
  ...require('./img-rule'),
}
