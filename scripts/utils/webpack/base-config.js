const webpack = require('webpack')
const paths = require('../paths')
const { nodeEnv } = require('../env')

const {
  getTsRule,
  getJsRule,
  getLessRule,
  getSvgRule,
  getImgRule,
} = require('./rules')
const { getHtmlPlugin, getDefinePlugin } = require('./plugins')

exports.getBaseConfig = () => {
  return {
    mode: nodeEnv,
    bail: false,
    target: 'web', // 升级webpack5 以后要配置这个
    // devtool: 'cheap-module-source-map',
    entry: { main: paths.appIndexJs },
    output: require('./output').getOutput(),
    resolve: require('./resolve').getResolve(),
    module: {
      // noParse: /^(vue|vue-router)$/,

      rules: [
        getTsRule(),
        // getVueRule(),
        getJsRule(),
        getLessRule(),
        // getSvgRule(),
        // getImgRule(),
      ],
    },
    plugins: [
      getHtmlPlugin(),

      // new webpack.HotModuleReplacementPlugin(),

      // You can remove this if you don't use Moment.js:
      new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),

      getDefinePlugin(),

      new (require('friendly-errors-webpack-plugin'))(), //日志打印插件
    ],

    // performance: false,
  }
}
