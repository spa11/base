// Plugin 会运行在 Preset 之前。
// Plugin 会从前到后顺序执行。
// Preset 的顺序则 刚好相反(从后向前)。
// ast tree
// travel
// ast tree
// gernarate

exports.getBebelConfig = () => {
  // api.cache.using(() => {
  //   return true
  // })

  const presets = []
  const comPlugins = []

  // 语法转换, 纳入规范的语法（ES2015, ES2016, ES2017, ES2018, Modules）所需要使用的plugins都包含在preset-env中。
  // 优点, entry全面导入, usage 按需导入
  // 缺点,如果打包忽略node_modules 时如果第三方包未转译则会出现兼容问题
  presets.push([
    require('@babel/preset-env').default,
    {
      targets: require('../browserslist').targets,
      useBuiltIns: 'usage', // 按需加载引入项 结合core-js@3
      corejs: { version: '3.8', proposals: false },
      loose: true,
      debug: true,
      // include: ['es.math.sign'],
      exclude: [
        // 'proposal-class-properties',
        // 'proposal-export-namespace-from', //
        // 'proposal-optional-chaining', // 可选链obj?.foo?.bar?.baz()
      ],
    },
  ])

  // 解析typescript
  // presets.push([require("@babel/preset-typescript").default, {}])

  // 最先要解析的是jsx语法
  // plugins.push([
  //   require("@vue/babel-plugin-jsx"),
  //   {
  //     mergeProps: false,
  //   },
  // ])

  // 自定义babel组件
  // plugins.push([require("./scripts/build/babelPlugins/abc")])

  // 处理import( ) 语法,  会引入两个模块"core-js/modules/es6.promise","core-js/modules/es6.array.iterator",
  // webpack5 已经很好的支持了该特性, 不需要额外配置
  // plugins.push([require("@babel/plugin-syntax-dynamic-import")])

  // comPlugins.push(
  //   [
  //     require('@babel/plugin-proposal-decorators'), // 装饰器插件
  //     { legacy: true }, // 配置兼容老语法
  //   ],
  //   [
  //     require('@babel/plugin-proposal-class-properties'), // 类属性插件
  //     { loose: true }, //值为true的时候, class 语法的编译用assign的方式, 否则用 Object.defineProperty
  //   ],
  // )

  // 借助 helper function 来实现特性的兼容，
  // 插件还能以沙箱垫片的方式防止污染全局， 并抽离公共的 helper function , 以节省代码的冗余
  // comPlugins.push([
  //   require('@babel/plugin-transform-runtime'),
  //   {
  //     regenerator: true,
  //     corejs: 3, // 指定 runtime-corejs 的版本，目前有 2 3 两个版本
  //     useESModules: true,
  //     helpers: true,
  //     absoluteRuntime: './node_modules',
  //     // version
  //   },
  // ])

  // 转换typescript, 功能同 ts-loader
  // plugins.push([
  //   require("@babel/plugin-transform-typescript"),
  //   {
  //     // jsxPragma,
  //     isTSX: false,
  //     allowNamespaces: false,
  //     allowDeclareFields: false,
  //   },
  // ])

  return {
    // webpack 打包的 library 是 module.exports 的方式，所以没添加这个配置时，使用 import 来加载 webpack 打包的 library 得到的是 undefined。
    // sourceType: "unambiguous",
    presets,
    // plugins,
    overrides: [
      {
        test: [/\.ts$/, /\.js$/],
        plugins: [...comPlugins],
      },
      // {
      //   test: [/\.tsx$/, /\.jsx$/],
      //   plugins: [
      //     // 最先要解析的是jsx语法
      //     [
      //       require('@vue/babel-plugin-jsx'),
      //       {
      //         mergeProps: false,
      //       },
      //     ],
      //     ...comPlugins,
      //   ],
      // },
    ],
  }
}
