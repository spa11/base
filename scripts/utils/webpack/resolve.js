const paths = require('../paths')

// resolve配置
exports.getResolve = () => {
  return {
    extensions: ['.ts', '.tsx', '.js', '.jsx', '.vue', '.json'],
    alias: {
      '@': paths.appSrc,
    },
  }
}
