const webpack = require('webpack')
const paths = require('../paths')
const { isDev, isPro } = require('../env')

exports.getHtmlPlugin = () => {
  const options = {
    template: paths.appHtml,
    inject: 'body',
    // templateParameters: function() {
    // },
  }
  if (isPro) {
    Object.assign(options, {
      minify: false, // TODO:
      // removeComments: false,
      // collapseWhitespace: false,
      // removeRedundantAttributes: false,
      // useShortDoctype: false,
      // removeEmptyAttributes: false,
      // removeStyleLinkTypeAttributes: false,
      // keepClosingSlash: false,
      // minifyJS: false,
      // minifyCSS: false,
      // minifyURLs: false,
    })
  }
  return new (require('html-webpack-plugin'))(options)
}

exports.getDefinePlugin = () => {
  return new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
    // 'process.env.APP_ENV': JSON.stringify('fat'),
    // 'process.env.TEST': JSON.stringify('test'),
  })
}
