const browserslist = require('browserslist')
// function process(source, opts) {
//   const browsers = browserslist(opts.overrideBrowserslist, {
//     stats: opts.stats,
//     path: opts.file,
//     env: opts.env,
//   })
//   // Your code to add features for selected browsers
// }
function getTargets() {
  const strs = [
    // '',
    // '> 1%',
    // 'edge>89',
    // 'defaults', // 表示> 0.5%, last 2 versions, Firefox ESR, not dead
    // 'last 2 versions',
    // 'not dead',
    'chrome>=89',
    // 'chrome>=66',

    // 'maintained node versions',
  ]
  return browserslist(strs)
}
const targets = getTargets()
// console.log('Browser List', targets)
exports.targets = targets
// console.log(getTargets())
