const paths = require('../paths')
const { isDev } = require('../env')

// 出口配置
exports.getOutput = () => {
  const obj = {
    path: paths.appDist,
    // 告诉 webpack 在 bundle 中引入「所包含模块信息」的相关注释。此选项默认值是 false，并且不应该用于生产环境(production)，但是对阅读开发环境(development)中的生成代码(generated code)极其有用。
    pathinfo: isDev,
    publicPath: '/', // 必须使用绝对路径, 否则子应用二级目录在开发环境不生效
  }
  if (isDev) {
    obj.filename = '[name].bundle.js'
    obj.chunkFilename = '[name].chunk.js'
  } else {
    obj.filename = 'js/[name].[contenthash:8].js'
    obj.chunkFilename = 'js/[name].[contenthash:8].chunk.js'
    // obj.libraryTarget = 'umd'
    // obj.library = `main-[name]`
    // obj.jsonpFunction = `webpackJsonp_main`
  }
  return obj
}
