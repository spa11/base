const { merge } = require('webpack-merge')
// const webpack = require('webpack')
const baseConfig = require('../utils/webpack').getBaseConfig()

// const { webpackConfig } = require('../utils/env')

module.exports = merge(baseConfig, {
  bail: true, // Stop compilation early in production
  // devtool: 'source-map',
  devtool: false,
  plugins: [
    // new (require('case-sensitive-paths-webpack-plugin'))(),
    new (require('mini-css-extract-plugin'))({
      filename: 'css/[name].[contenthash:8].css',
      chunkFilename: 'css/[name].[contenthash:8].css',
    }),

    // new webpack.NormalModuleReplacementPlugin(),
    // new webpack.NormalModuleReplacementPlugin(
    //   /some\/path\/config\.development\.js/,
    //   './config.production.js',
    // ),
    // 当开启 HMR 的时候使用该插件会显示模块的相对路径/
    // new webpack.NamedModulesPlugin(),

    new (require('webpack-manifest-plugin').WebpackManifestPlugin)({
      fileName: 'asset-manifest.json',
      // publicPath: paths.publicUrlOrPath,
      generate: (seed, files, entrypoints) => {
        const manifestFiles = files.reduce((manifest, file) => {
          manifest[file.name] = file.path
          return manifest
        }, seed)
        const entrypointFiles = entrypoints.main.filter(
          (fileName) => !fileName.endsWith('.map'),
        )
        return {
          files: manifestFiles,
          entrypoints: entrypointFiles,
        }
      },
    }),

    // 打包分析组件
    // webpackConfig.analyzer &&
    new (require('webpack-bundle-analyzer').BundleAnalyzerPlugin)({
      analyzerMode: 'static', // 生成report.html
      openAnalyzer: false, // 是否自动打开浏览器
      generateStatsFile: true,
      statsFilename: 'webpack-stats.json',
      // 控制那些资源应该被排除
      // excludeAssets: assetName => {
      //   console.log("assetName", assetName)
      //   return true
      // },
    }),
  ].filter(Boolean),
  optimization: {
    // minimize: true ,
    minimize: false, // TODO
    // usedExports: true,
    // Automatically split vendor and commons
    moduleIds: 'named', // 模块id用相对路径
    chunkIds: 'named',
    splitChunks: {
      chunks: 'all',
      // chunks: "async",
      minSize: 30000,
      minChunks: 1,
      maxAsyncRequests: 5,
      maxInitialRequests: 3,
      automaticNameDelimiter: '~',
      // name: true,
      cacheGroups: {
        corejs: {
          minSize: 1,
          test: /[\\/]node_modules[\\/]core-js[\\/]/,
          name: 'vendor/core-js',
        },
        // lodash: {
        //   minSize: 1,
        //   test: /[\\/]node_modules[\\/]lodash[\\/]/,
        //   name: 'vendor/lodash',
        // },
        // 提取所有的 svg sprite
        // spriteSvg: {
        //   minSize: 1,
        //   test: /[\\/]src[\\/]svg[\\/]common[\\/]/,
        //   name: 'svg-common',
        // },
        // 提取vue全家桶
        // vue: {
        //   minChunks: 1,
        //   test: /\/node_modules\/vue(-router)?\//,
        //   name: 'vendor/vue-family',
        // },
        vendors: {
          name: 'chunk-vendors',
          test: /[\\/]node_modules[\\/]/,
          priority: -10,
          chunks: 'initial',
        },
        common: {
          name: 'chunk-common',
          minChunks: 2,
          priority: -20,
          chunks: 'initial',
          reuseExistingChunk: true,
        },
      },
    },
    // https://github.com/facebook/create-react-app/issues/5358
    runtimeChunk: {
      name: (entrypoint) => `runtime-${entrypoint.name}`,
    },
  },
})
