// const webpack = require('webpack')
const { merge } = require('webpack-merge')

const baseConfig = require('../utils/webpack').getBaseConfig()

module.exports = merge(baseConfig, {
  devtool: 'cheap-module-source-map',
  plugins: [
    // new (require('case-sensitive-paths-webpack-plugin'))({ debug: false }),
    // new webpack.HotModuleReplacementPlugin(),
  ],
})
