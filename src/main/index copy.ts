import {
  registerMicroApps, //
  runAfterFirstMounted,
  setDefaultMountApp,
  start,
  initGlobalState,
} from 'qiankun'
import '../styles/index.less'
import { init } from './init'

/**
 * Step1 初始化应用（可选）
 */
// render({ loading: true });

const { el } = init()

const loader = (loading: boolean) => {
  console.log('loading', loading)
}

registerMicroApps(
  [
    {
      name: 'vue',
      // entry: 'http://localhost:8000',
      entry: 'http://localhost:1991',

      // entry: 'http://localhost:7101',
      container: el.contentEl,
      loader,
      // activeRule: (location: Location) => {
      //   return location.pathname.indexOf('/vue') === 0
      // },
      activeRule: '/vue',
    },
  ],
  {
    beforeLoad: [
      (app: any) => {
        console.log('[LifeCycle] before load %c%s', 'color: green;', app.name)
        return Promise.resolve()
      },
    ],
    afterMount: [],
    afterUnmount: [],
  },
)

const { onGlobalStateChange, setGlobalState } = initGlobalState({
  user: 'qiankun',
})
onGlobalStateChange((value, prev) => {
  console.log('[onGlobalStateChange - master]:', value, prev)
})
setGlobalState({
  ignore: 'master',
  user: {
    name: 'master',
  },
})

/**
 * Step3 设置默认进入的子应用
 */
setDefaultMountApp('/vue')

/**
 * Step4 启动应用
 */
start()

runAfterFirstMounted(() => {
  console.log('[MainApp] first app mounted')
})
