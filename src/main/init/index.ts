import style from './main.module.less'
/**
 * 初始化
 */
export function init() {
  const appEl = document.getElementById('app') as HTMLDivElement
  const boxEl = document.createElement('div')
  boxEl.classList.add(style.box)
  const headerEl = document.createElement('header')
  headerEl.classList.add(style.header)

  const contentEl = document.createElement('div')
  contentEl.classList.add(style.content)
  boxEl.appendChild(headerEl)
  boxEl.appendChild(contentEl)
  appEl.appendChild(boxEl)
  return {
    el: {
      appEl,
      headerEl,
      contentEl,
      boxEl,
    },
  }
}
