import { LifeCycles } from 'single-spa'

import { LcProps } from './types'
export let loaderAssets: (lcProps: LcProps) => Promise<LifeCycles<any>>

if (process.env.NODE_ENV === 'development') {
  loaderAssets = loaderDevAssets
} else {
  loaderAssets = loaderProdAssets
}

/**
 * 开发环境文件加载
 */
async function loaderDevAssets(lcProps: LcProps) {
  const entry = `${lcProps.url}/main.bundle.js`
  await loaderFile(entry)

  return formatLifeCycle(lcProps)
}
/*
 * 生产环境文件加载模块
 */
async function loaderProdAssets(lcProps: LcProps) {
  const { url, name } = lcProps
  const entry = `${url}/manifest.js?${new Date().getTime()}`
  console.log('加载文件', entry)

  await loaderFile(entry)
  const manifest = window.__manifest
  delete window.__manifest
  // console.log(manifest)
  if (!manifest) {
    throw new Error(
      `${url},name: ${name}, 没有给出文件资源资源对象, window.__manifest数据为空`,
    )
  }
  const entrypoints = manifest.entrypoints
  const promises: Promise<any>[] = []
  entrypoints.forEach((entryStr) => {
    if (entryStr.substr(-3) === 'css') return
    const src = `${url}/${entryStr}`
    promises.push(loaderFile(src))
  })
  await Promise.all(promises)

  return formatLifeCycle(lcProps)
}

/**
 * 格式化生命周期
 * @param lifeCycle
 */
function formatLifeCycle(lcProps: LcProps) {
  const lifeCycle = (window as any).__spaLifecycle
  if (!lifeCycle) {
    throw new Error(
      `地址:${lcProps.url},App:'${lcProps.name}'没有指定生命周期函数, 请在子应用App中指定window.__spaLifecycle`,
    )
  }
  const tempLifecycle = new Object(null) as LifeCycles<any>

  const lcNames = ['bootstrap', 'mount', 'unmount']
  for (let i = 0; i < lcNames.length; i++) {
    const key = lcNames[i] as keyof LifeCycles
    const lc = lifeCycle[key]
    if (typeof lc !== 'function') errLifeCycle(lcProps, key, lc)
    tempLifecycle[key] = (config: LcProps) => {
      return run(config, lc)
    }
  }
  if (lifeCycle.update) {
    tempLifecycle.update = (config: LcProps) => {
      return run(config, lifeCycle.update)
    }
  }

  return tempLifecycle
}
function errLifeCycle(config: LcProps, lifeCyclesStr: string, lc: any) {
  throw new Error(
    `应用${config.name},加载地址${config.url}后面, 'win.__spaLifecycle' 的 '${lifeCyclesStr}'生命周期不是函数,而是 ${lc}`,
  )
}

function run(lcProps: LcProps, fn: (config?: LcProps) => unknown) {
  const result = fn(lcProps) as any
  if (result?.then) return result as Promise<any>
  return Promise.resolve(result)
}

/**
 * 加载文件
 * @param src 文件路径
 * @returns
 */
function loaderFile(src: string) {
  return new Promise((res) => {
    __webpack_require__.l(src, () => {
      res(null)
    })
  })
}
