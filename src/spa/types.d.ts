import { LifeCycleFn, AppProps } from 'single-spa'

export type CustomProps = {
  activeWhen: string
  url: string
  el: HTMLDivElement
}
export type LcProps = CustomProps & AppProps
