import { registerApplication, start, LifeCycles, AppProps } from 'single-spa'
import { loaderAssets } from './loader'
import { LcProps, CustomProps } from './types'

const win = window as any
win.__rg_wqd__ = true // 微前端标志, 用于给子应用使用

const apps = [
  {
    name: 'app1',
    // url: 'http://localhost:1991',  // 生产环境
    url: 'http://localhost:8000', // 开发环境
    activeWhen: '/vue',
  },
]

export function init(el: HTMLDivElement) {
  apps.forEach((config) => {
    const { name, url, activeWhen } = config
    const customProps = { activeWhen, url, el } as LcProps
    registerApplication<LcProps>({
      name,
      app: applicationFn,
      activeWhen,
      customProps,
    })
  })
  return start
}

async function applicationFn(lcProps: LcProps) {
  const lc = await loaderAssets(lcProps)
  console.log('生命周期', lc)

  return lc
}
