import './styles/index.less'

import { el } from './main'

import { init } from './spa'

console.log(el)

const start = init(el.contentEl)
start()
