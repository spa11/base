/* eslint-disable @typescript-eslint/no-empty-interface */

import Vue, { VNodeData as VueVNodeData } from 'vue'

declare global {
  interface Window {
    vm: Vue
    __manifest:
      | {
          entrypoints: string[]
          files: Record<string, string>
        }
      | undefined
    [key: 'string']: any
  }

  type VNode = Vue.VNode
  type VNodeData = VueVNodeData

  // eslint-disable-next-line no-undef
  // type Component = ComponentOptions | typeof Vue | AsyncComponent

  namespace JSX {
    interface Element {} // jsx标签指向的函数必须返回VNode

    interface ElementAttributesProperty {
      props // 指定用来使用的属性名
    }
    interface ElementClass extends Vue {}
    interface IntrinsicElements {
      [elem: string]: {
        class?: string | string[]
        [elem: string]: any
      }
    }
  }
}
